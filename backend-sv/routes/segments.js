var express = require('express');
var router = express.Router();
var url = require('url');

/* GET users listing. */
router.get('/', function(req, res, next) {
    var queryData = url.parse(req.url, true).query;
    var mysql = require('mysql');
    var config = {
        host: "localhost",
        user: "root",
        password: "root",
        database: "segVideo"
    }
    var connection = mysql.createConnection(config);
    connection.connect();

    connection.query("SELECT s.name,s.frame_cut,s.duration\n" +
        "\t\tFROM project p\n" +
        "\t\tINNER JOIN algorithm_project ap\n" +
        "\t\tON p.project_id = ap.project_id AND p.`name` =    \n" +
        "\t\tINNER JOIN algorithm a \n" +
        "\t\tON a.`name` = pAlgorithm AND ap.`algorithm_id` = a.`algorithm_id`\n" +
        "\t\tINNER JOIN segment s\n" +
        "\t\tON s.algorithm_project_id = ap.algorithm_project_id;",
        function(err,rows,fields){
            if (!err){
                if (rows.length > 0){
                    var row = {projects:rows}
                    res.send(row);
                } else {
                    var error = {msg:'No projects'}
                    res.send(error);
                }
            } else {
                var error = {msg:'Error'}
                res.send(error);
            }
        });
});

module.exports = router;
